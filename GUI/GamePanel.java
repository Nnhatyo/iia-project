package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import Game.PlateauCam;

public class GamePanel extends JPanel implements MouseListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// dimensions
	public static int WIDTH = 350;
	public static int HEIGHT = 650;
	public static int size = HEIGHT / PlateauCam.LIGNES; 
	
	// graphic
	private Graphics2D g;
	private BufferedImage screen;
	private BufferedImage bK, wK, bP, wP;
	private Color background = Color.BLACK;
	private Point clickedCell = null;
	
	//
	private PlateauCam plateau;

	//
	private BufferedImage[] cases = new BufferedImage[2];
	
	public GamePanel(PlateauCam p) {
		plateau = p;
		init();
	}

	private void init() {		
		addMouseListener(this);
		screen = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) screen.getGraphics();
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setFocusable(true);
		requestFocus();
		
		// Initialising cells
				cases[0] = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
				cases[1] = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
				
				Graphics2D g2d = (Graphics2D) cases[0].getGraphics();
				g2d.setColor(Color.DARK_GRAY);
				g2d.fillRect(0, 0, size , size);
				
				g2d = (Graphics2D) cases[1].getGraphics();
				g2d.setColor(Color.LIGHT_GRAY);
				g2d.fillRect(0, 0, size , size);
				
		// Initialising piece's image
				try {
					wK = ImageIO.read(getClass().getResource("/whiteKnight.png"));
					wP = ImageIO.read(getClass().getResource("/whitePiece.png"));
					bK = ImageIO.read(getClass().getResource("/blackKnight.png"));
					bP = ImageIO.read(getClass().getResource("/blackPiece.png"));
				} catch (Exception e) {
					e.printStackTrace();
				}
	}
	
	public void draw(Graphics g2) {
		g.setColor(background);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		for (int y = 0; y < PlateauCam.LIGNES; ++y) {
			for (int x = 0; x < PlateauCam.COLONNES; ++x) {
				// A B C D E F G
				// 0 1 2 3 4 5 6
				// - - - B - - - | 0
				// - - B N B - - | 1
				// - B N B N B - | 2
				// B N B N B N B | 3
				// N B N B N B N | 4
				// B N B N B N B | 5
				// N B N B N B N | 6
				// B N B N B N B | 7
				// N B N B N B N | 8
				// B N B N B N B | 9
				// - B N B N B - | 10
				// - - B N B - - | 11
				// - - - B - - - | 12
				// cases[i+j%2] -> i+j%2 == 0, Noire
				if (plateau.getCase(y, x) != PlateauCam.NULL) {
					BufferedImage cell = cases[(x+y)%2];
					g.drawImage(cell, x*size, y*size, size, size,null);
					switch (plateau.getCase(y, x)) {
						case PlateauCam.CAVALIER_BLANC :
							g.drawImage(wK, x*size, y*size, size, size,null);
							break;
						case PlateauCam.CAVALIER_NOIR :
							g.drawImage(bK, x*size, y*size, size, size,null);
							break;
						case PlateauCam.PION_BLANC :
							g.drawImage(wP, x*size, y*size, size, size,null);
							break;
						case PlateauCam.PION_NOIR :
							g.drawImage(bP, x*size, y*size, size, size,null);
							break;
						default :
							break;
					}
				}
			}
		}
		if (clickedCell != null) {
			g.setColor(new Color(0, 0, 90, 128));
			g.fillRect((int) clickedCell.getX() * size, (int) clickedCell.getY() * size, size, size);
		}
		g2.drawImage(screen, 0, 0, WIDTH, HEIGHT, null);
	}

	private void update(Point point) {
		int x = (int) (point.getX()) / size;
		int y = (int) (point.getY()) / size;
//		System.out.println("Ligne: " + y + " | colonnes: " + x);
//		System.out.println("\t" + plateau.getCase(y, x));
		if (x >= PlateauCam.COLONNES || y >= PlateauCam.LIGNES || x < 0 || y < 0 || plateau.getCase(y, x) == PlateauCam.NULL) {
			clickedCell = null;
		} else {
			clickedCell = new Point(x, y);
		}
	}
	
	@Override
	public void paint(Graphics g) {
		draw(g);
//		super.paint(g);
	}
	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		update(e.getPoint());
		repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

}
