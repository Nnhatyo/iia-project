package GUI;

import javax.swing.JFrame;

import Game.PlateauCam;

public class GameFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PlateauCam plateau;
	private GamePanel pan;
	
	public GameFrame(PlateauCam p) {
		plateau = p;
		pan = new GamePanel(plateau);
		init();
		display();
	}

	private void display() {
		setVisible(true);
	}

	private void init() {
		setTitle("Cam");
		setContentPane(pan);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		pack();
	}
	
	public static void main(String[] args) {
		GameFrame cam = new GameFrame(new PlateauCam());
	}

	public void update() {
		repaint();
	}
}
