package Game;

import java.util.ArrayList;
import java.util.Scanner;

public class Menu{
	
	public static void demo(){
		PlateauCam demonstration = new PlateauCam();
		System.out.println(demonstration.toString());
		{
			ArrayList<String> moves = new ArrayList<String>();
			ArrayList<String> descMove = new ArrayList<String>();
			moves.add("D10-D9");
			descMove.add("On teste si on peut déplacer le contenu d'une case vide.");
			moves.add("C09-C11");
			descMove.add("On teste si un pion peut canter.");
			moves.add("B09-A09");
			descMove.add("On teste si un pion peut aller vers la gauche.");
			moves.add("B09-A10");
			descMove.add("On teste si un pion peut aller en bas à gauche.");
			moves.add("B09-B10");
			descMove.add("On teste si un pion peut reculer.");
			moves.add("F09-G10");
			descMove.add("On teste si un pion peut aller en bas à droite.");
			moves.add("F09-G09");
			descMove.add("On teste si un pion peut aller à droite.");
			moves.add("C10-C9");
			descMove.add("On teste si un cavalier peut aller sur une case occupée.");
			moves.add("C10-C11");
			descMove.add("On teste si on peut déplacer un cavalier vers le bas.");
			moves.add("C11-D11");
			descMove.add("On teste si on peut déplacer un cavalier vers la droite.");
			moves.add("D11-E12");
			descMove.add("On teste si on peut déplacer un cavalier en bas à gauche.");
			moves.add("E12-F12");
			descMove.add("On teste si on peut déplacer un cavalier en-dehors du plateau.");
			moves.add("E12-D13");
			descMove.add("On teste si on peut déplacer un cavalier sur son château.");
			moves.add("E10-E11");
			descMove.add("On prépare le test pour l'enchaînement de plusieurs canter.");
			moves.add("E12-E10-E08");
			descMove.add("On teste si un cavalier peut canter plusieurs fois.");
			moves.add("E12-E10");
			descMove.add("On teste si un cavalier peut canter une seule fois.");
			moves.add("E11-E08");
			descMove.add("On teste si un cavalier peut canter par-dessus plusieurs pièces.");
			moves.add("E10-E08");
			descMove.add("On fait un deuxième canter pour préparer le test de capture");
			moves.add("E08-C06");
			descMove.add("On teste si un cavalier peut se déplacer de plusieurs cases d'un seul coup.");
			moves.add("E07-D07-C06");
			descMove.add("On teste si un cavalier peut se déplacer de plusieurs cases d'un seul coup (autre méthode possible).");
			moves.add("E08-D07");
			descMove.add("On déplace un cavalier en diagonale pour préparer une capture multiple.");
			moves.add("D07-D06");
			descMove.add("On déplace un cavalier en avant pour préparer une capture multiple.");
			moves.add("D06-D07");
			descMove.add("On teste si un cavalier peut ne pas faire une capture.");
			moves.add("D06-D04-D03");
			descMove.add("On teste si un cavalier peut faire un mouvement simple après une capture.");
			moves.add("D06-D04-F04-F06-D04-B04-B06-D04");
			descMove.add("On fait une capture multiple avec un cavalier.");
			moves.add("q");
			descMove.add("On teste si quand le joueur écrit \"q\" le programme s'arrête.");
			
			String player = "blanc";
			
			int size = moves.size(), i=0;
			Scanner sc = new Scanner(System.in);
			String move = moves.get(i);
			while (!move.equals("q") && i < size){
				System.out.println("----------------------------------------------------------------------");
				System.out.print("Votre action : ");
				System.out.println(move);
				System.out.println(descMove.get(i));
				i++;
				
				System.out.println("Appuyez sur Entrée pour voir l'incidence du coup sur le jeu.");
				sc.nextLine();
				if (demonstration.estValide(move, player)) {
					demonstration.play(move, player);
					System.out.println(demonstration.toString());
				} else {
					System.out.println(demonstration.toString());
					System.out.println("[ Coup non valide ]");
				}
				move = moves.get(i);
			}
			System.out.println("Test de la fonction finDePartie():");
			System.out.println("La fonction a renvoyé la valeur: " + demonstration.finDePartie("blanc"));
			System.out.println("\nLa démonstration est terminée. Fin du programme.");
			sc.close();
		}
	}
	
	public static void jeux(PlateauCam jeuxCam){
		System.out.println(jeuxCam.toString());
		{
			String move;
			String player = new String();
			player = "blanc";
			Scanner sc = new Scanner(System.in);
			System.out.println("C'est au tour du joueur "+player+" de jouer.");
			System.out.print("Votre action: ");
			move = sc.next();
			while (!move.equals("q") && !jeuxCam.finDePartie(player)) {
				if (jeuxCam.estValide(move, player)) {
					jeuxCam.play(move, player);
					System.out.println(jeuxCam.toString());
					if(player == "blanc")
						player = "noir";
					else
						player = "blanc";
				}
				System.out.println("C'est au tour du joueur "+player+" de jouer.");
				System.out.print("Votre action: ");
				move = sc.next();
			}
			sc.close();
		}
		jeuxCam.saveToFile(jeuxCam.fileName);
		System.out.println("Votre partie a bien été sauvegardée à l'emplacement suivant : "+jeuxCam.fileName+".");
	}
	
	public static void configure(String fileName, int choice){
		PlateauCam jeuxCam = new PlateauCam();
		if(choice == 1){
			jeuxCam.fileName = fileName;
			jeux(jeuxCam);
		}
		else if(choice == 2){
			jeuxCam = new PlateauCam(fileName);
			jeux(jeuxCam);
		}
		else{
			System.err.println("Le choix effectué n'est pas valide.\nTerminaison du programme.");
			System.exit(-1);
		}
	}
	
	public static void testCoupsPossibles() {
		byte v = PlateauCam.VIDE;
		byte B = PlateauCam.CAVALIER_BLANC;
		byte N = PlateauCam.CAVALIER_NOIR;
		byte b = PlateauCam.PION_BLANC;
		byte n = PlateauCam.PION_NOIR;
		byte X = PlateauCam.NULL;
		byte CB = PlateauCam.CHATEAU_BLANC;
		byte CN = PlateauCam.CHATEAU_NOIR;
		
		{
			int stop;
			Scanner sc = new Scanner(System.in);
			ArrayList<String> coups;
			
			do {
				byte[][] plateau = {	{ X,  X,  X, CN, X, X, X},
										{ X,  X,  B,  v, b, X, X},
										{ X,  v,  v,  v, v, v, X},
										{ v,  v,  N,  v, v, v, v},
										{ v,  n,  n,  n, n, n, v},
										{ v,  v,  v,  v, v, v, v},
										{ v,  v,  v,  v, v, v, v},
										{ v,  v,  v,  v, v, v, v},
										{ v,  b,  b,  b, b, b, v},
										{ v,  v,  B,  v, v, v, v},
										{ X,  v,  v,  v, v, v, X},
										{ X,  X,  N,  v, B, X, X},
										{ X,  X,  X, CB, X, X, X}};

				PlateauCam jeuxCam = new PlateauCam(plateau);

				System.out.println(jeuxCam.toString());
				coups = jeuxCam.coupsPossibles("blanc");
				System.out.println("Joueur blanc a " + coups.size() + " coups possibles:");
				for (String coup : coups) {
					System.out.println("|" + coup + " |");
				}
				
				coups = jeuxCam.coupsPossibles("noir");
				System.out.println("Joueur noir a " + coups.size() + " coups possibles:");
				for (String coup : coups) {
					System.out.println("\t" + coup);
				}
			} while ( (stop = sc.nextInt()) != 0 );
			sc.close();
		}
		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args){
		int choice = 0;
		Scanner sc = new Scanner(System.in);
		while(choice > 4 || choice < 1){
			System.out.println("Bonjour, et bienvenue dans ce jeu de Cam.");
			System.out.println("Que voulez-vous faire ?\n1. Commencer une nouvelle partie.\n2. Continuer une partie enregistrée.\n3. Regarder une partie de démonstration.\n4. Test coups possibles.");
			choice = sc.nextInt();
			System.out.println();
		}
		if(choice == 1){
			System.out.println("Veuillez nous indiquer le chemin auquel vous souhaiteriez sauvegarder votre partie.");
			String fileName = new String();
			fileName = sc.next();
			if(fileName.endsWith("/")||fileName.endsWith("\\"))
				fileName += "camelot.sav";
			configure(fileName,choice);
		}
		else if(choice == 2){
			System.out.println("Veuillez nous indiquer le chemin de votre fichier de sauvegarde.");
			String fileName = new String();
			fileName = sc.next();
			configure(fileName,choice);
		}
		else if(choice == 3){
			System.out.print("La démonstration va commencer.\nCette démonstration jouera uniquement des coups pour le joueur blanc.\nSi vous choisissez de jouer normalement plus tard, l'alternance des deux joueurs sera effective.\nSi le programme indique qui sera le prochain joueur sans afficher le plateau, cela veut dire que le coup précédemment spécifié n'est pas valide.\nAppuyez sur Entrée pour continuer.\n");
			sc.nextLine();
			sc.nextLine();
			demo();
		}
		else if (choice == 4) {
			testCoupsPossibles();
		}
		else{
			sc.close();
			System.exit(0);
		}
	}
}