package Game;

public class Tools {

	public static byte colToInt(char col) {
		switch (col) {
			case 'A' :
				return 0;
			case 'B' :
				return 1;
			case 'C' :
				return 2;
			case 'D' :
				return 3;
			case 'E' :
				return 4;
			case 'F' :
				return 5;
			case 'G' :
				return 6;
			default :
				return -1;
		}
	}
	
	public static String colToString(int col) {
		switch (col) {
			case 0 :
				return "A";
			case 1 :
				return "B";
			case 2 :
				return "C";
			case 3 :
				return "D";
			case 4 :
				return "E";
			case 5 :
				return "F";
			case 6 :
				return "G";
			default :
				return String.valueOf(col);
		}
	}
	
	public static byte charToByte(char c, int i){
		switch(c){
			case '-' : return PlateauCam.VIDE;
			case ' ' : return PlateauCam.NULL;
			case 'n' : return PlateauCam.PION_NOIR;
			case 'b' : return PlateauCam.PION_BLANC;
			case 'N' : return PlateauCam.CAVALIER_NOIR;
			case 'B' : return PlateauCam.CAVALIER_BLANC;
			case '*' : if(i==1) return PlateauCam.CHATEAU_NOIR;else if (i==13) return PlateauCam.CHATEAU_BLANC;
			default : return PlateauCam.NULL;
		}
	}
	
	public static char ByteToChar(byte b){
		switch(b) {
			case PlateauCam.VIDE : return '-';
			case PlateauCam.NULL : return ' ';
			case PlateauCam.PION_BLANC : return 'b';
			case PlateauCam.PION_NOIR : return 'n';
			case PlateauCam.CAVALIER_BLANC : return 'B';
			case PlateauCam.CAVALIER_NOIR : return 'N';
			default : return '*';
		}
	}
	
	public static byte[][] cellsFromMove(String move, byte[][] plateau) {
		String[] spl = move.split("-");
		byte[][] cells = new byte[spl.length][2];
		for (int j = 0; j < spl.length; j++) {
			byte col = colToInt(Character.toUpperCase(spl[j].charAt(0)));
			byte  row = (byte) (Byte.parseByte(spl[j].substring(1)) - 1);
			
			// (plateau[row][col] & XNULL) give us the value of the XNULL bit.
			if ((PlateauCam.getCell(row, col, plateau) & PlateauCam.M_NULL) != 0)
				return null;
			else {
				cells[j][0] = row;
				cells[j][1] = col;
			}
		}
		return cells;
	}
	
	public static byte[][] copiePlateau(byte[][] p) {
		byte[][] copy = new byte [PlateauCam.LIGNES][PlateauCam.COLONNES];
		for (int i = 0; i < PlateauCam.LIGNES; i++) {
			for (int j = 0; j < PlateauCam.COLONNES; j++) {
				copy[i][j] = p[i][j];
			}
		}
		return copy;
	}
	
	public static boolean isACapture(int dx, int dy, int iRow, int iCol, int eRow, int eCol, byte player, byte[][] plateau) {		
		byte inter = PlateauCam.getCell(iRow, iCol, plateau);
		if (dy == 0 && (dx == 2 || dx == -2)) {
			// left or right capture
			if (PlateauCam.mask(inter, PlateauCam.M_NULL) == 0) {
				// inter cell is not null
				if ((PlateauCam.mask(inter, PlateauCam.M_VIDE)) == 0) {
					// inter cell is not empty
					if ( PlateauCam.mask((inter ^ player), PlateauCam.M_BLANC) != 0 ) {
						// inter contains an enemy piece
						// return end cell is empty or not
						return ( ( (PlateauCam.getCell(eRow, eCol, plateau) ^ PlateauCam.M_VIDE) & 
								PlateauCam.M_NULLVIDE ) == 0);
					}
				}
			}
		} else if (dy == 2 || dy == -2) {
			if (dx == 0 || dx == 2 || dx == -2) {
				if (PlateauCam.mask(inter, PlateauCam.M_NULL) == 0) {
					// inter cell is not null
					if ((PlateauCam.mask(inter, PlateauCam.M_VIDE)) == 0) {
						// inter cell is not empty
						if ( PlateauCam.mask((inter ^ player), PlateauCam.M_BLANC) != 0 ) {
							// inter contains an enemy piece
							// return end cell is empty or not
							return ( ( (PlateauCam.getCell(eRow, eCol, plateau) ^ PlateauCam.M_VIDE) & 
									PlateauCam.M_NULLVIDE ) == 0);
						}
					}
				}
			}
		}
		return false;
	}

	public static boolean isAMove(int dx, int dy, int eRow, int eCol, byte player, byte[][] plateau) {
		if (dy == 0 && (dx == 1 || dx == -1)) {
			// we can move if the end cell is empty and not a castle
			// Explanation:
			// if cell = 000a bcde, where:
			// 'a' is the CHATEAU bit (mask = 0001 0000)
			// 'c' is the VIDE bit (mask = 0000 0100)
			// cell & (VIDE | CHATEAU) give us 00a0 c000
			// and 00a0 c000 XOR 0000 0100 give us 0000 0000 if CHATEAU bit is 0 and VIDE bit is 1
			// in other words if the cell is empty and not our castle
			if ( ((PlateauCam.getCell(eRow, eCol, plateau) & PlateauCam.M_BLANC) ^ player) == 0 )
				return (((PlateauCam.getCell(eRow, eCol, plateau) & PlateauCam.M_VIDE) ^ PlateauCam.M_VIDE) == 0);
			else
				return (((PlateauCam.getCell(eRow, eCol, plateau) & PlateauCam.M_VIDE) ^ PlateauCam.M_VIDE) == 0);
		} else if (dy == 1 || dy == -1) {
			if (dx == 0 || dx == 1 || dx == -1) {				
				// we can move if the end cell is empty and not our castle
				if ( ((PlateauCam.getCell(eRow, eCol, plateau) & PlateauCam.M_BLANC) ^ player) == 0 )
					return (((PlateauCam.getCell(eRow, eCol, plateau) & PlateauCam.M_VIDE) ^ PlateauCam.M_VIDE) == 0);
				else
					return (((PlateauCam.getCell(eRow, eCol, plateau) & PlateauCam.M_VIDE) ^ PlateauCam.M_VIDE) == 0);
			}
		}
		return false;
	}

	public static boolean isACanter(int dx, int dy, int iRow, int iCol, int eRow, int eCol, byte player, byte[][] plateau) {		
		if (dy == 0 && (dx == 2 || dx == -2)) {
			// if we jump an ally, we canter
			if ( ((PlateauCam.getCell(iRow, iCol, plateau) & PlateauCam.M_BLANC) ^ player) == 0 ) {
				return (((PlateauCam.getCell(eRow, eCol, plateau) & (PlateauCam.M_VIDE | PlateauCam.M_CHATEAU)) ^ PlateauCam.M_VIDE) == 0);
			}
		} else if (dy == 2 || dy == -2) {
			if (dx == 0 || dx == 2 || dx == -2) {
				// if we jump an ally, we canter
				if ( ((PlateauCam.getCell(iRow, iCol, plateau) & PlateauCam.M_BLANC) ^ player) == 0 ) {
					return (((PlateauCam.getCell(eRow, eCol, plateau) & (PlateauCam.M_VIDE | PlateauCam.M_CHATEAU)) ^ PlateauCam.M_VIDE) == 0);
				}
			}
		}
		return false;
	}

	public static void capture(int sRow, int sCol,int iRow, int iCol, int eRow, int eCol, byte player, byte[][] plateau) {
		plateau[eRow][eCol] = PlateauCam.getCell(sRow, sCol, plateau);
		plateau[iRow][iCol] = PlateauCam.M_VIDE;
		plateau[sRow][sCol] = PlateauCam.M_VIDE;
		System.out.println("Capture");
	}

	public static void move(int sRow, int sCol, int eRow, int eCol, byte player, byte[][] plateau) {
		plateau[eRow][eCol] = PlateauCam.getCell(sRow, sCol, plateau);
		plateau[sRow][sCol] = PlateauCam.M_VIDE;
		System.out.println("Move");
	}

	public static void canter(int sRow, int sCol, int eRow, int eCol, byte player, byte[][] plateau) {
		plateau[eRow][eCol] = PlateauCam.getCell(sRow, sCol, plateau);
		plateau[sRow][sCol] = PlateauCam.M_VIDE;
		System.out.println("Canter");
	}

	// return true if a capture is possible from the cell [sRow, sCol]
	public static boolean canCapture(int sRow, int sCol, byte player, byte[][] plateau) {
		byte inter;
		// checking for captures
		for (byte r = -1; r < 2; ++r) {
			for(byte c = -1; c < 2; ++c) {
				inter = PlateauCam.getCell(sRow+r, sCol+c, plateau);
				if ((inter & (PlateauCam.M_VIDE | PlateauCam.M_NULL)) == 0) {
					if ( ((inter ^ player) & (PlateauCam.M_BLANC | PlateauCam.M_NULL)) != 0 ) {
					// 	inter is not a null cell and contain an enemy piece
						if (isACapture((2*c), (2*r), sRow+r, sCol+c, sRow + (2*r), sCol + (2*c), player, plateau)) {
							System.out.println("capture vers [" + (sRow + (2*r)) + ", " + (sCol + (2*c)) + "]");
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}