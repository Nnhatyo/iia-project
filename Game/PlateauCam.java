package Game;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PlateauCam implements Partie1 {
	/////////////////////////////////////////////////
	// Static attributes
	public final static int COLONNES = 7;
	public final static int LIGNES = 13;
	// MASKS
	// 0000 0001 -> bit de couleur, 1 = blanc, 0 = noir
	public final static byte M_BLANC = 0x1;
	// 0000 0010 -> bit de type, 1 = pion, 0 = cavalier
	public final static byte	M_PION = 0x2;
	// 0000 0100 -> 1 = case vide, 0 = case pleine
	public final static byte M_VIDE = 0x4;
	// 0000 1000 -> 1 = case hors jeu, 0 = case valide
	public final static byte M_NULL = 0x8;
	// 0001 0000 -> 1 = case chateau, 0 = case normale
	public final static byte M_CHATEAU = 0x10;
	public final static byte M_NULLVIDE = M_VIDE | M_NULL;
	
	public final static byte NULL = M_NULL;
	public final static byte VIDE = M_VIDE;
	
	public final static byte PION_BLANC = M_PION | M_BLANC;
	public final static byte PION_NOIR = M_PION;
	
	public final static byte CAVALIER_BLANC = M_BLANC;
	public final static byte CAVALIER_NOIR = 0;
	
	public final static byte CHATEAU_BLANC = M_CHATEAU | M_BLANC | M_VIDE;
	public final static byte CHATEAU_NOIR = M_CHATEAU | M_VIDE;
	
	// Areas
	/////////////////////////////////////////////////
	// ---------- East area ----------
	public int eX1 = 5;
	public int eX2 = 7;
	public int eY1 = 4;
	public int eY2 = 9;
	/////////////////////////////////////////////////
	// ---------- West area ----------
	public int wX1 = 0;
	public int wX2 = 2;
	public int wY1 = 4;
	public int wY2 = 9;
	/////////////////////////////////////////////////
	// ---------- Center area ----------
	public int cX1 = 2;
	public int cX2 = 5;
	public int cY1 = 4;
	public int cY2 = 9;
	/////////////////////////////////////////////////
	// ---------- North area ----------
	public int nX1 = 0;
	public int nX2 = 7;
	public int nY1 = 0;
	public int nY2 = 4;
	/////////////////////////////////////////////////
	// ---------- South area ----------
	public int sX1 = 0;
	public int sX2 = 7;
	public int sY1 = 9;
	public int sY2 = 13;

	/////////////////////////////////////////////////
	// ---------- Attributes ----------
	public String fileName = new String();
	private byte plateau[][] = new byte [LIGNES][COLONNES];
	// Liste de pions
	@SuppressWarnings("unchecked")
	ArrayList<Piece>[] pieces = new ArrayList[2]; // 1: white pieces, 0: black pieces
	
	/////////////////////////////////////////////////
	// ---------- Counters ----------	
	public int nbWhite = 0;
	public int nbWhiteE = 0;
	public int nbWhiteW = 0;
	public int nbWhiteC = 0;
	public int nbWhiteN = 0;
	public int nbWhiteS = 0;
	public int distanceWhite = 0;
	
	public int nbBlack = 0;
	public int nbBlackE = 0;
	public int nbBlackW = 0;
	public int nbBlackC = 0;
	public int nbBlackN = 0;
	public int nbBlackS = 0;
	public int distanceBlack = 0;
	
	/////////////////////////////////////////////////
	// ---------- Constructors ----------	
	public PlateauCam(PlateauCam p) {
		this.plateau = Tools.copiePlateau(p.getPlateau());
	}
	
	public PlateauCam(byte[][] plateau) {
		this.plateau = plateau;
	}
	
	public PlateauCam(String fileName){
		setFromFile(fileName);
		this.fileName = fileName;
	}

	public PlateauCam(){
		pieces[0] = new ArrayList<>();
		pieces[1] = new ArrayList<>();
		
		for(int i = 0 ; i <= (LIGNES-3)/2 ; i++){
			for(int j = 0 ; j < COLONNES ; j++){
				switch(i){
					case 0 :
						plateau[0][j] = (j == 3) ?  CHATEAU_NOIR : NULL;
						plateau[12][j] = (j == 3) ? CHATEAU_BLANC : NULL;
						break;
					case 1 :
						plateau[1][j] = (j>=2 && j<=4) ? VIDE : NULL;
						plateau[11][j] = (j>=2 && j<=4) ? VIDE : NULL;
						break;
					case 2 :
						plateau[2][j] = (j>=1 && j<=5) ? VIDE : NULL;
						plateau[10][j] = (j>=1 && j<=5) ? VIDE : NULL;
						break;
					case 3 :
						plateau[3][j] = (j==2 || j==4) ? CAVALIER_NOIR : VIDE;
						plateau[9][j] = (j==2 || j==4) ? CAVALIER_BLANC : VIDE;
						break;
					case 4 :
						plateau[4][j] = (j>0 && j<6) ? PION_NOIR : VIDE;
						plateau[8][j] = (j>0 && j<6) ? PION_BLANC : VIDE;
						break;
					default :
						plateau[5][j]=VIDE;
						plateau[6][j]=VIDE;
						plateau[7][j]=VIDE;
						break;
				}
			}
		}
		for (int i = 1; i < 6; i++) {
			pieces[1].add(new Piece(8, i)); pieces[0].add(new Piece(4, i)); // Pions
		}
		
		pieces[1].add(new Piece(9, 2)); pieces[0].add(new Piece(3, 2)); // Cavaliers
		pieces[1].add(new Piece(9, 4)); pieces[0].add(new Piece(3, 4)); // Cavaliers
	}
	
	/////////////////////////////////////////////////
	// ---------- File initialization ----------
	public boolean fileIsValid(byte[][] plateau){
		boolean isGood=true;
		int i = 0,j=0,pN = 0, pB = 0, cN = 0, cB = 0;
		while(isGood && i<LIGNES){
			j=0;
			while(isGood && j<COLONNES){
				isGood &= (plateau[i][j]!='X');
				j++;
			}
			i++;
		}
		if(isGood){
			for(i = 0 ; i < LIGNES ; i++){
				for(j = 0 ; j < COLONNES ; j++){
					switch(plateau[i][j]){
						case PION_BLANC : pB++;break;
						case CAVALIER_BLANC : cB++;break;
						case PION_NOIR : pN++;break;
						case CAVALIER_NOIR : cN++;break;
						default : break;
					}
				}
			}
			isGood = (pB < 6 && pN < 6 && cB < 3 && cN < 3);
			isGood &= (plateau[0][3] != PION_NOIR && plateau[0][3] != CAVALIER_NOIR && plateau[12][3] != PION_BLANC && plateau[12][3] != CAVALIER_BLANC);
		}
		return(isGood);
	}
	
	public void setFromFile(String fileName){
		byte plateau[][] = new byte[LIGNES][COLONNES];
		int i=0,j,taille_fichier=0;
		try{
			InputStream flux = new FileInputStream(fileName); 
			InputStreamReader lecture = new InputStreamReader(flux);
			BufferedReader buff = new BufferedReader(lecture);
			buff.mark(8192);
			String ligne;
			while((ligne=buff.readLine())!=null){
				if(!ligne.startsWith("%"))
					if(ligne.length() == 13)
							taille_fichier++;
			}
			if(taille_fichier > 12){
				buff.reset();
				char number[] = new char[2];
				while((ligne=buff.readLine())!=null){
					if(!ligne.startsWith("%")){
						if(ligne.length() == 13){
							number[0] = ligne.charAt(0);
							number[1] = ligne.charAt(1);
							if(number[0] != ligne.charAt(11) || number[1] != ligne.charAt(12)){
								System.err.println("Le fichier donné en paramètre n'est pas conforme.\nTerminaison du programme.");
								System.exit(3);
							}
							i = Integer.parseInt(new String(number));
							if(i>0 && i<14){
								char cur_case = '\0';
								for(j=0;j<COLONNES;j++){
									cur_case = ligne.charAt(j+3);
									plateau[i-1][j] = Tools.charToByte(cur_case, i);
								}
							}
						}
					}
				}
			}
			else{
				System.err.println("Le fichier donné en paramètre n'est pas complet.\nTerminaison du programme.");
				System.exit(1);
			}
			buff.close(); 
		}		
		catch (Exception e){
			e.printStackTrace();
		}
		
		if(!fileIsValid(plateau)){
			System.err.println("Le fichier donné en paramètre ne correspond pas à une partie valide.\nTerminaison du programme.");
			System.exit(2);
		}
		this.plateau=plateau;
	}

	public void saveToFile(String fileName){
		String plateau = new String();
		Locale locale = Locale.FRENCH;
		DateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm",locale);
		plateau += "%Partie du "+simpleDateFormat.format(new Date())+"\n";
		plateau += this.toString("");
		try{
			PrintWriter printer = new PrintWriter(new BufferedWriter(new FileWriter(fileName)));
			printer.print(plateau);
			printer.close();
		}
		catch(Exception e){
			System.err.println("Le chemin spécifié pour la sauvegarde du fichier est invalide. Le fichier sera sauvegardé dans le répertoire courant sous le nom de cam.sav.");
			saveToFile("./cam.sav");
			System.exit(-2);
		}
	}
	
	/////////////////////////////////////////////////
	// ----------  ----------
	public boolean estValide(String move, String player) {
		switch (player) {
			case "blanc" :
				return estValide(Tools.cellsFromMove(move, plateau), M_BLANC, this.plateau);
			case "noir" :
				return estValide(Tools.cellsFromMove(move, plateau), (byte) 0, this.plateau);
			default :
				return false;
		}
	}
	
	public boolean estValide(String move, String player, byte[][] plateau) {
		switch (player) {
			case "blanc" :
				return estValide(Tools.cellsFromMove(move, plateau), M_BLANC, plateau);
			case "noir" :
				return estValide(Tools.cellsFromMove(move, plateau), (byte) 0, plateau);
			default :
				return false;
		}
	}
	
	public boolean estValide(byte[][] cells, byte player, byte[][] plateau) {
		byte[][] copyPlateau = Tools.copiePlateau(plateau);
		byte start = 0, end = 0;
		// s = start cell
		// e = end cell
		int sRow, sCol, eRow, eCol;
		
		if (cells == null)
			return false;

		// Check if the piece to move belong to the player
		start = plateau[cells[0][0]][cells[0][1]];
		
		// with "& M_BLANC" we have the value of the color bit.
		// If first piece's color bit and player's color bit are different (XOR != 0)
		// the piece does not belong to the player.
		if ( (mask(start, M_BLANC) ^ player) != 0)
			return false;
		
		// i = intermediate (cell between start and end)
		int iCol, iRow;
		int dx, dy;
		boolean captured = false, moved = false;
		
		// Checking the piece's path
		for (int i = 1; i < cells.length; ++i) {
			sRow = cells[i-1][0]; 	sCol = cells[i-1][1];
			eRow = cells[i][0]; 	eCol = cells[i][1];
			dx = eCol - sCol; 		dy = eRow - sRow;
			iRow = sRow + ((eRow - sRow) >> 1);
			iCol = sCol + ((eCol - sCol) >> 1);
			
			start = getCase(sRow, sCol);
			end = getCase(eRow, eCol);
			
			// If end cell is empty, we check if the play is valid
			// if we moved, we can't link with another play
			if ((mask(end, M_VIDE) != 0) && !moved) {
				if (Tools.canCapture(sRow, sCol, player, copyPlateau)) {
					// a capture is possible
					if (Tools.isACapture(dx, dy, iRow, iCol, eRow, eCol, player, copyPlateau)) {
						captured = true;
						Tools.capture(sRow, sCol,iRow, iCol, eRow, eCol, player, copyPlateau);
					} else {
						// a capture is possible but the move is not a capture
						return false;
					}
				} else {
					// MOVE
					if (!captured && Tools.isAMove(dx, dy, eRow, eCol, player, copyPlateau)) {
						if (mask(start, M_PION) != 0) {
							// Check if the pawn is moving in the right direction
							if ( (mask(start, M_BLANC) != 0) && (dy != -1) ) {
								return false;
							} else if ( (mask(start, M_BLANC) == 0) && (dy != 1) ) {
								return false;
							}
						}
						moved = true;
						Tools.move(sRow, sCol, eRow, eCol, player, copyPlateau);
				
					// CANTER
					} else if (!captured && Tools.isACanter(dx, dy, iRow, iCol, eRow, eCol, player, copyPlateau)) {
						if (mask(start, M_PION) != 0) {
							// Pawns can't canter
							return false;
						}
						moved = true;
						Tools.canter(sRow, sCol, eRow, eCol, player, copyPlateau);
					} else {
						// Not a valid move
						return false;
					}
				}
			} else {
				// end cell is not empty
				return false;
			}
		}
		if (captured && Tools.canCapture(cells[cells.length - 1][0], cells[cells.length - 1][1], player, copyPlateau)) {
			// if we can capture after the last move
			return false;
		} else {
			// The move chain is valid
			return true;
		}
	}
	
	/////////////////////////////////////////////////
	// ---------- Possible moves ----------
	public ArrayList<String> coupsPossibles(String player) {
		switch (player) {
			case "blanc" :
				return coupsPossibles(M_BLANC, player);
			case "noir" :
				return coupsPossibles((byte) 0, player);
			default :
				return new ArrayList<String>();
		}
	}
	
	private ArrayList<String> coupsPossibles(byte player, String splayer) {
		return coupsPossibles(player, splayer, this.plateau);
	}
	
	private ArrayList<String> coupsPossibles(byte player, String splayer, byte[][] plateau) {
		ArrayList<String> coups = new ArrayList<String>();
		ArrayList<String> captures = new ArrayList<String>();
		
		// Check if the player should move from his castle
		byte castle;
		switch (splayer) {
			case "blanc" :
				castle = mask(plateau[12][3], M_CHATEAU);
				if (castle == 0) {
					coupsPossibles(12, 3, plateau, coups, captures);
					if (captures.size() == 0)
						return coups;
					else
						return captures;
				}
			case "noir" :
				castle = mask(plateau[0][3], M_CHATEAU);
				if (castle == 0) {
					coupsPossibles(0, 3, plateau, coups, captures);
					if (captures.size() == 0)
						return coups;
					else
						return captures;
				}
				break;
			default :
				return coups;
		}
		
		// If castles are empty, check for possible plays
//		for (Piece piece : pieces[player]) {
//			coupsPossibles(piece.row, piece.col, plateau, coups, captures);
//		}

		if (player == M_BLANC) {
			// we begin near ennemy's castle
			for (int i = 0; i < LIGNES; i++) {
				for (int j = 0; j < COLONNES; j++) {
					if (mask(getCase(i, j), M_VIDE) == 0) {
						// the cell is not empty
						if (mask(getCase(i, j) ^ player, M_BLANC) == 0) {
							// [i, j] belong to the player
							coupsPossibles(i, j, plateau, coups, captures);
						}
					}
				}
			}
		} else {
			for (int i = LIGNES-1; i >= 0; i--) {
				for (int j = 0; j < COLONNES; j++) {
					if (mask(getCase(i, j), M_VIDE) == 0) {
						// the cell is not empty
						if (mask(getCase(i, j) ^ player, M_BLANC) == 0) {
							// [i, j] belong to the player
							coupsPossibles(i, j, plateau, coups, captures);
						}
					}
				}
			}
		}
		
		if (captures.size() == 0)
			return coups;
		else
			return captures;
	}

	private boolean coupsPossibles(int row, int col, byte[][] plateau, ArrayList<String> coups, ArrayList<String> captures) {
		String initCell = String.valueOf(Tools.colToString(col));
		initCell += String.valueOf(row+1);
		String coup = "";

		byte piece = getCell(row, col, plateau);
		byte player = mask(piece, M_BLANC);
		byte p;
		int eRow, eCol;
		int moveDirection = (player == M_BLANC) ? -1 : 1;
		
		if (piece == NULL || piece == VIDE) {
			return false;
		} else {
			// put possible captures in the captures list
			for (byte dr = -1; dr < 2; ++dr) {
				for(byte dc = -1; dc < 2; ++dc) {
					eRow = row + (2*dr);
					eCol = col + (2*dc);
					if (Tools.isACapture((2*dc), (2*dr), row+dr, col+dc, eRow, eCol, player, plateau)) {
						// Si on trouve une capture, on regarde si on peut encore capturer après.
						coup = Tools.colToString(col) + String.valueOf(row + 1) + "-" +
						Tools.colToString(eCol) + String.valueOf(eRow + 1);
						String iPath = "-" + Tools.colToString(eCol) + String.valueOf(eRow + 1);
						byte[][] copy = Tools.copiePlateau(plateau);
						play(coup, player, copy);
						capturesPossibles(new String(initCell+iPath), player, eRow, eCol, copy, captures);
					}
				}
			}
			if (captures.size() == 0) {
				// Here, there are no possible captures
				// checking for moves
				if ( mask(piece, M_PION) != 0) {
					// The piece is a pawn
					for (int c = -1; c < 2; ++c) {
						p = getCell(row+moveDirection, col+c, plateau);
						if (mask(p, M_NULL) == 0) {
							// end cell is not null
							if ( (mask(piece, M_BLANC) != 0) && (moveDirection == -1) ) {
								// The pawn moves in the right direction (White pawn)
								if (mask(p, M_CHATEAU) != 0) {
									// end cell is a castle
									if (mask(p ^ player, M_BLANC) != 0) {
										// end cell is ennemy's castle
										coups.clear();
										coup = initCell;
										coup += "-" + String.valueOf(Tools.colToString(col+c));
										coup += String.valueOf(row+moveDirection+1);
										coups.add(coup);
										return true;
									}
								} else {
									// end cell is not a castle
									if ((mask(p, M_VIDE)) != 0) {
										// end cell is empty
										coup = initCell;
										coup += "-" + String.valueOf(Tools.colToString(col+c));
										coup += String.valueOf(row+moveDirection+1);
										coups.add(coup);
									}
								}
							} else if ( (mask(piece, M_BLANC) == 0) && (moveDirection == 1) ) {
								// The pawn moves in the right direction (Black pawn)
								if (mask(p, M_CHATEAU) != 0) {
									// end cell is a castle
									if (mask(p ^ player, M_BLANC) != 0) {
										// end cell is ennemy's castle
										coups.clear();
										coup = initCell;
										coup += "-" + String.valueOf(Tools.colToString(col+c));
										coup += String.valueOf(row+moveDirection+1);
										coups.add(coup);
										return true;
									}
								} else {
									// end cell is not a castle
									if ((mask(p, M_VIDE)) != 0) {
										// end cell is empty
										coup = initCell;
										coup += "-" + String.valueOf(Tools.colToString(col+c));
										coup += String.valueOf(row+moveDirection+1);
										coups.add(coup);
									}
								}
							}
						}
					}
				} else {
					// The piece is a knight
					// checking for moves
					for (int c = -1; c < 2; ++c) {
						for (int r = -1; r < 2; ++r) {
							eRow = row+r;
							eCol = col+c;
							
							p = getCell(eRow, eCol, plateau);
							if (mask(p, M_NULL) == 0) {
								// end cell is not null
								if (mask(p, M_CHATEAU) != 0) {
									// end cell is a castle
									if (mask(p ^ player, M_BLANC) != 0) {
										// end cell is ennemy's castle
										coups.clear();
										coup = initCell;
										coup += "-" + String.valueOf(Tools.colToString(col+c));
										coup += String.valueOf(row+moveDirection+1);
										coups.add(coup);
										return true;
									}
								} else {
									if ((mask(p, M_VIDE)) != 0) {
										coup = initCell;
										coup += "-" + String.valueOf(Tools.colToString(eCol));
										coup += String.valueOf(eRow+1);
										coups.add(coup);
									}
								}
							}
						}
					}

					// checking for canters
					for (int r = -1; r < 2; ++r) {
						for(int c = -1; c < 2; ++c) {
							p = getCell(row+r, col+c, plateau);
							if ((mask(p ^ player, (M_BLANC | M_VIDE))) == 0 ) {
								// inter cell is an ally
								eCol = col+(2*c);
								eRow = row+(2*r);
								p = getCell(eRow, eCol, plateau);
//								if ((mask(p, M_VIDE) ^ M_VIDE) == 0) {
								if (mask(getCell(eRow, eCol, plateau) ^ M_VIDE ^ player, (M_VIDE | M_CHATEAU)) == 0) {
									// end cell is empty and not our castle
									coup = initCell;
									coup += "-" + String.valueOf(Tools.colToString(eCol));
									coup += String.valueOf(eRow+1);
									coups.add(coup);
								}
							}
						}
					}
				}
			}
		}
		return false;
	}

	private void capturesPossibles(String path, byte player, int sRow, int sCol, byte[][] plateau, ArrayList<String> captures) {
		int eRow, eCol, iRow, iCol;
		boolean noCaptures = true;
		for (byte r = -1; r < 2; ++r) {
			for(byte c = -1; c < 2; ++c) {
				eRow = sRow + (2*r);
				eCol = sCol + (2*c);
				iCol = sCol+c;
				iRow = sRow+r;
				if (Tools.isACapture((2*c), (2*r), iRow, iCol, eRow, eCol, player, plateau)) {
					// Si on trouve une capture, on regarde si on peut encore capturer après.
					noCaptures= false;
					String coup = Tools.colToString((byte)sCol) + String.valueOf(sRow + 1) + "-";
					coup += Tools.colToString((byte)eCol);
					coup += String.valueOf(eRow + 1);

					//								if (!path.contains(coup))
					String iPath = "-" + Tools.colToString((byte)eCol) + String.valueOf(eRow + 1);
					byte[][] copy = Tools.copiePlateau(plateau);
					play(coup, player, copy);
					capturesPossibles(new String(path+iPath), player, eRow, eCol, copy, captures);
				}
			}
		}
		// If no more captures are found, and we have at least 1 capture, we add the move to the list
		if (noCaptures)
			captures.add(path);
	}

	/////////////////////////////////////////////////
	// ---------- Play methods ----------
	public void play(String move, String player) {
		switch (player) {
			case "blanc" :
				play(Tools.cellsFromMove(move, plateau), M_BLANC, this.plateau);
				break;
			case "noir" :
				play(Tools.cellsFromMove(move, plateau), (byte) 0, this.plateau);
				break;
			default :
				break;
		}
	}
	
	public PlateauCam playRet(String move, String player) {
		switch (player) {
			case "blanc" :
				return play(Tools.cellsFromMove(move, plateau), M_BLANC, this.plateau);
			case "noir" :
				return play(Tools.cellsFromMove(move, plateau), (byte) 0, this.plateau);
			default :
				return this;
		}
	}
	
	public PlateauCam play(String move, String player, byte[][] plateau) {
		switch (player) {
			case "blanc" :
				play(Tools.cellsFromMove(move, plateau), M_BLANC, plateau);
				return this;
			case "noir" :
				play(Tools.cellsFromMove(move, plateau), (byte) 0, plateau);
				return this;
			default :
				return this;
		}
	}
	
	public PlateauCam play(String move, byte player, byte[][] plateau) {
		return play(Tools.cellsFromMove(move, plateau), player, plateau);
	}
	
	public PlateauCam play(byte[][] cells, byte player, byte[][] plateau) {
		if (cells == null) { return this;}
		
		byte start;
		
		// s = start cell
		// e = end cell
		// i = intermediate (cell between start and end)
		int iCol, iRow, sRow, sCol, eRow, eCol;
		
		for(int i = 1; i < cells.length; ++i) {
			start = getCell(cells[i-1][0], cells[i-1][1], plateau);
			sRow = cells[i-1][0];
			sCol = cells[i-1][1];
			eRow = cells[i][0];
			eCol = cells[i][1];
			// In Java, -1/2 = -1 ... so we're forced to divide a double by 2
			// and apply floor.
			double diff = (eRow - sRow) / 2;
			diff = (diff >= 0) ? diff : Math.floor(diff);
			iRow = sRow + (int) diff;
			
			diff = (eCol - sCol) / 2;
			diff = (diff >= 0) ? diff : Math.floor(diff);
			iCol = sCol + (int) diff;

			plateau[eRow][eCol] = start;
			plateau[sRow][sCol] = M_VIDE;
			
			if (iCol != sCol || iRow != sRow) {
				// If capturing, we remove the captured piece
				if ( (mask(getCell(iRow, iCol, plateau) ^ player, M_BLANC)) != 0) {
					plateau[iRow][iCol] = M_VIDE;
				}
			}
		}
		return this;
	}
	
	/////////////////////////////////////////////////
	// ----------  ----------
	public boolean finDePartie(byte player) {
		switch (player) {
			case M_BLANC :
				return finDePartie("blanc");
			case (~M_BLANC) :
				return finDePartie("noir");
			default :
				return false;
		}
	}
	
	public boolean win(String player) {
		switch (player) {
			case "blanc" :
				return ((mask(plateau[0][3], M_BLANC) != 0) || (coupsPossibles("noir").size() == 0));
			case "noir" :
				return ((mask(plateau[12][3], M_BLANC) == 0) || (coupsPossibles("blanc").size() == 0));
			default :
				return false;
		}
	}
	
	public boolean finDePartie(String player) {
		if (mask(plateau[12][3], M_BLANC) == 0 || mask(plateau[0][3], M_BLANC) != 0) {
			return true;
		}
		
		if(player.equals("blanc")) {
			return (coupsPossibles("blanc").size() == 0);
		}
		else {
			return (coupsPossibles("noir").size() == 0);
		}
	}
	
	/////////////////////////////////////////////////
	// ---------- Utilities ----------
	public String toString(){
		return toString(" ");
	}
	
	public String toString(String sep){
		String splateau = new String();
		splateau = "%  A"+sep+"B"+sep+"C"+sep+"D"+sep+"E"+sep+"F"+sep+"G"+"\n";
		for(int i = 0 ; i < LIGNES ; i++){
			splateau+=String.format("%02d ",(i+1));
			for(int j = 0 ; j < COLONNES ; j++){
				short cur_case = plateau[i][j];
				char representedBy;
					switch(cur_case){
					case VIDE : representedBy = '-';break;
					case NULL : representedBy = ' ';break;
					case PION_BLANC : representedBy = 'b';break;
					case PION_NOIR : representedBy = 'n';break;
					case CAVALIER_BLANC : representedBy = 'B';break;
					case CAVALIER_NOIR : representedBy = 'N';break;
					default : representedBy = '*';break;
				}
				splateau+=representedBy;
				if(j!=COLONNES-1)
					splateau += sep;
			}
			splateau+=String.format(" %02d\n",(i+1));
		}
		splateau += "%  A"+sep+"B"+sep+"C"+sep+"D"+sep+"E"+sep+"F"+sep+"G"+"\n";
		return splateau;
	}
	
	public static String plToString(String sep, byte[][] plateau){
		String splateau = new String();
		splateau = "%  A"+sep+"B"+sep+"C"+sep+"D"+sep+"E"+sep+"F"+sep+"G"+"\n";
		for(int i = 0 ; i < LIGNES ; i++){
			splateau+=String.format("%02d ",(i+1));
			for(int j = 0 ; j < COLONNES ; j++){
				short cur_case = plateau[i][j];
				char representedBy;
					switch(cur_case){
					case VIDE : representedBy = '-';break;
					case NULL : representedBy = ' ';break;
					case PION_BLANC : representedBy = 'b';break;
					case PION_NOIR : representedBy = 'n';break;
					case CAVALIER_BLANC : representedBy = 'B';break;
					case CAVALIER_NOIR : representedBy = 'N';break;
					default : representedBy = '*';break;
				}
				splateau+=representedBy;
				if(j!=COLONNES-1)
					splateau += sep;
			}
			splateau+=String.format(" %02d\n",(i+1));
		}
		splateau += "%  A"+sep+"B"+sep+"C"+sep+"D"+sep+"E"+sep+"F"+sep+"G"+"\n";
		return splateau;
	}
	public byte getCase(int row, int col) {
		return getCell(row, col, this.plateau);
	}
	
	public static byte getCell(int row, int col, byte[][] plateau) {
		if (col < 0 || row < 0 || col >= COLONNES || row >= LIGNES) {
			return NULL;
		} else {
			return plateau[row][col];
		}
	}
	
	public PlateauCam copy() {
		return new PlateauCam(this);
	}
	
	public static byte mask(byte b, byte m) {
		return (byte) (b & m);
	}
	
	public static byte mask(byte b, int m) {
		return (byte) (b & m);
	}
	
	public static byte mask(int b, int m) {
		return (byte) (b & m);
	}
	
	public void countPieces() {
		byte piece;
		
		nbWhite = 0;
		nbWhiteC = 0;
		nbWhiteE = 0;
		nbWhiteN = 0;
		nbWhiteS = 0;
		nbWhiteW = 0;
		distanceWhite = Integer.MAX_VALUE;
		
		nbBlack = 0;
		nbBlackC = 0;
		nbBlackE = 0;
		nbBlackN = 0;
		nbBlackS = 0;
		nbBlackW = 0;
		distanceBlack = Integer.MAX_VALUE;
		
		/////////////////////////////////////////////////
		// ---------- East area ----------	
		for (int i = eX1; i < eX2; i++) {
			for (int j = eY1; j < eY2; j++) {
				piece = getCase(j, i);
				if (mask(piece, M_VIDE) == 0) {
					if ( mask(piece, M_BLANC) != 0 ) {
						nbWhite++;
						nbWhiteE++;
						if (j < distanceWhite)
							distanceWhite = j;
					} else {
						nbBlack++;
						nbBlackE++;
						if (13-j < distanceBlack)
							distanceBlack = (13 - j);
					}
				}
			}
		}
		/////////////////////////////////////////////////
		// ---------- West area ----------
		for (int i = wX1; i < wX2; i++) {
			for (int j = wY1; j < wY2; j++) {
				piece = getCase(j, i);
				if (mask(piece, M_VIDE) == 0) {
					if ( mask(piece, M_BLANC) != 0 ) {
						nbWhite++;
						nbWhiteW++;
						if (j < distanceWhite)
							distanceWhite = j;
					} else {
						nbBlack++;
						nbBlackW++;
						if (13-j < distanceBlack)
							distanceBlack = (13 - j);
					}
				}
			}
		}
		/////////////////////////////////////////////////
		// ---------- Center area ----------
		for (int i = cX1; i < cX2; i++) {
			for (int j = cY1; j < cY2; j++) {
				piece = getCase(j, i);
				if (mask(piece, M_VIDE) == 0) {
					if ( mask(piece, M_BLANC) != 0 ) {
						nbWhite++;
						nbWhiteC++;
						if (j < distanceWhite)
							distanceWhite = j;
					} else {
						nbBlack++;
						nbBlackC++;
						if (13-j < distanceBlack)
							distanceBlack = (13 - j);
					}
				}
			}
		}
		/////////////////////////////////////////////////
		// ---------- North area ----------
		for (int i = nX1; i < nX2; i++) {
			for (int j = nY1; j < nY2; j++) {
				piece = getCase(j, i);
				// Here we should check if the cell is null
				if (mask(piece, (M_VIDE | M_NULL | M_CHATEAU)) == 0) {
					if ( mask(piece, M_BLANC) != 0 ) {
						nbWhite++;
						nbWhiteN++;
						if (j < distanceWhite)
							distanceWhite = j;
					} else {
						nbBlack++;
						nbBlackN++;
						if (13-j < distanceBlack)
							distanceBlack = (13 - j);
					}
				}
			}
		}
		/////////////////////////////////////////////////
		// ---------- South area ----------
		for (int i = sX1; i < sX2; i++) {
			for (int j = sY1; j < sY2; j++) {
				piece = getCase(j, i);
				// Here we should check if the cell is null
				if (mask(piece, (M_NULLVIDE | M_CHATEAU)) == 0) {
					// cell is not null, not empty, and not a castle
					if ( mask(piece, M_BLANC) != 0 ) {
						nbWhite++;
						nbWhiteS++;
						if (j < distanceWhite)
							distanceWhite = j;
					} else {
						nbBlack++;
						nbBlackS++;
						if (13-j < distanceBlack)
							distanceBlack = (13 - j);
					}
				}
			}
		}
	}
	
	/////////////////////////////////////////////////
	// ---------- Getters/Setters ----------
	public byte[][] getPlateau() {
		return plateau;
	}
}