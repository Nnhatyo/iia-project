package Game;

public class Piece {
	int row;
	int col;
	
	public Piece(int r, int c) {
		row = r;
		col = c;
	}
	
	public void setCol(int col) {
		this.col = col;
	}
	
	public void setRow(int row) {
		this.row = row;
	}
	
	public int getCol() {
		return col;
	}
	
	public int getRow() {
		return row;
	}
}
