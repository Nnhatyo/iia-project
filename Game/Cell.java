package Game;

public class Cell {
	
	public int row;
	public int col;
	public char cCol;
	public byte piece;
	
	public Cell(String r, char c) {
		col = colToInt(c);
		row = Integer.parseInt(r) - 1;
		cCol = c;
	}
	
	public Cell(String r, char c, byte p) {
		col = colToInt(c);
		row = Integer.parseInt(r) - 1;
		cCol = c;
		piece = p;
	}
	
	public String toString() {
		String str = "";

		str += cCol;
		str += String.valueOf(row+1);
		return str;
	}
	
	public static int colToInt(char col) {
		switch (col) {
			case 'A' :
				return 0;
			case 'B' :
				return 1;
			case 'C' :
				return 2;
			case 'D' :
				return 3;
			case 'E' :
				return 4;
			case 'F' :
				return 5;
			case 'G' :
				return 6;
			default :
				return -1;
		}
	}
	
	public static char intToCol(int col) {
		switch (col) {
			case 0 :
				return 'A';
			case 1 :
				return 'B';
			case 2 :
				return 'C';
			case 3 :
				return 'D';
			case 4 :
				return 'E';
			case 5 :
				return 'F';
			case 6 :
				return 'G';
			default :
				return 'Z';
		}
	}
}
