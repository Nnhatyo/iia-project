package Game;
public interface Partie1 {
	
	/** initialise un plateau à partir d'un fichier texte
	* @param fileName le nom du fichier à lire
	*/
	public void setFromFile(String fileName);
	
	/** sauve la configuration d'un plateau dans d'un fichier
	* @param fileName le nom du fichier à sauvegarder
	*
	* Le format doit être compatible avec celui utilisé pour la lecture.
	*/
	public void saveToFile(String fileName);
	
	/** indique si le coup <move> est valide pour le joueur <player> sur le plateau courant
	* @param move le coup à jouer
	* @param player le joueur qui joue (représenté par "noir" ou "blanc")
	*/
	public boolean estValide(String move, String player);
	
	/** modifie le plateau en jouant le coup move
	* @param move le coup à jouer, représenté sous la forme "A1-B2-C3"
	* @param player le joueur qui joue représenté par "noir" ou "blanc"
	*/
	public void play(String move, String player);
	
	/** vrai lorsque le plateau corespond à une fin de partie
	*/
	public boolean finDePartie(String player);
}
