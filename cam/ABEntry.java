package cam;

// alphaBeta entry
public class ABEntry {
	Node node;
	int profondeur;
	
	public ABEntry(Node n, int p) {
		node = n;
		profondeur = p;
	}
	
	@Override
	public int hashCode() {
		return (node.hashCode() + profondeur);
	}
	
	public boolean equals(Object other) {
		ABEntry e = (ABEntry) other;
		return (e.node.equals(node) && (e.profondeur == profondeur));
	}
}
