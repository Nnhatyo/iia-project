package cam;

import Game.PlateauCam;

public class Strat2 implements Heuristique {

	/////////////////////////////////////////////////
	// ---------- Strategy ----------
	private static int castleRush = 5;
	private static int castleDefense = 1;
	private static int defense = 3;
	private static int offense = 2;
	private static int side = 0;
	private static int center = 0;



	public int eval(PlateauCam p, byte player, int nbCoups) {
		//System.out.println("On evalue:");
		//System.out.println(p.toString());

		int heuristique = 0;
		if ( player == PlateauCam.M_BLANC ) {
			// Heuristique for white player
			{
				heuristique += defense * p.nbWhite;
				heuristique += offense * p.nbBlack;
				heuristique += side * p.nbWhiteC;
				heuristique += center * (p.nbBlackE + p.nbBlackW);
				heuristique += castleDefense * p.distanceBlack;
				heuristique += castleRush * p.distanceWhite;
			}
		} else {
			// Heuristique for black player
			{
				heuristique += defense;
				heuristique += offense;
				heuristique += side * p.nbBlackC;
				heuristique += center * (p.nbWhiteE + p.nbWhiteW);
				heuristique += castleDefense * p.distanceWhite;
				heuristique += castleRush * p.distanceBlack;
			}
		}
		//System.out.println("heuristique: " + heuristique);
		return heuristique;
	}

	@Override
	public int eval(PlateauCam p, IJoueur player, int nbCoups) {
		if (player.getNumJoueur() == IJoueur.BLANC) {
			return eval(p, PlateauCam.M_BLANC, nbCoups);
		} else {
			return eval(p, (byte) ~PlateauCam.M_BLANC, nbCoups);
		}
	}

	public int eval(PlateauCam p, String player, int nbCoups) {
		if (player.equals("blanc")) {
			return eval(p, PlateauCam.M_BLANC, nbCoups);
		} else {
			return eval(p, (byte) ~PlateauCam.M_BLANC, nbCoups);
		}
	}

	@Override
	public void updateStrategy(PlateauCam p, String player) {
		p.countPieces();

		castleRush = 5;
		castleDefense = 1;
		defense = 2;
		offense = 3;
		side = 2;
		center = 1;

		switch (player) {
		case "blanc":
			//{
//			castleRush = (p.distanceBlack != 0) ? p.distanceWhite / p.distanceBlack : -p.distanceWhite;
//			castleDefense = (p.distanceWhite != 0) ? p.distanceBlack / p.distanceWhite : -p.distanceBlack;
//			defense = (p.nbBlack != 0) ? p.nbWhite / p.nbBlack : 0;
//			offense = (p.nbWhite != 0) ? p.nbBlack / p.nbWhite : 0;
//			side = ((p.nbBlackE + p.nbBlackW) != 0) ? p.nbBlackC / (p.nbBlackE + p.nbBlackW) : 0;
//			center = (p.nbBlackC != 0) ? (p.nbBlackE + p.nbBlackW) / p.nbBlackC : 0;
			//}
			break;
		case "noir":
			//{
//			castleRush = (p.distanceWhite != 0) ? p.distanceBlack / p.distanceWhite : -p.distanceBlack;
//			castleDefense = (p.distanceBlack != 0) ? p.distanceWhite / p.distanceBlack : -p.distanceWhite;
//			defense = (p.nbWhite != 0) ? p.nbBlack / p.nbWhite : 0;
//			offense = (p.nbBlack != 0) ? p.nbWhite / p.nbBlack : 0;
//			side = ((p.nbWhiteE + p.nbWhiteW) != 0) ? p.nbWhiteC / (p.nbWhiteE + p.nbWhiteW) : 0;
//			center = (p.nbWhiteC != 0) ? (p.nbWhiteE + p.nbWhiteW) / p.nbWhiteC : 0;
			//}
			break;
		default:
			break;
		}
	}
}
