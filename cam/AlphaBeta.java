package cam;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;

import Game.PlateauCam;

public class AlphaBeta {

	/** La profondeur de recherche par d�faut
     */
    private final static int PROFMAXDEFAUT = 4;
    private int coupe = 0;
    private PlateauCam best = new PlateauCam();
   
    // -------------------------------------------
    // Attributs
    // -------------------------------------------
 
    /**  La profondeur de recherche utilis�e pour l'algorithme
     */
    private int profMax = PROFMAXDEFAUT;

     /**  L'heuristique utilis�e par l'algorithme
      */
   private Heuristique h;

    /** Le joueur Min
     *  (l'adversaire) */
    private String joueurMin;

    /** Le joueur Max
     * (celui dont l'algorithme de recherche adopte le point de vue) */
    private String joueurMax;
    
    /** Le meilleur coup après déroulage de l'algorithme
     * 
     */
    private String bestMove;
    
    public Integer heurMax;
    
    private HashMap<ABEntry, Integer> heurMap = new HashMap<>();
    
    private Long startingTime;
    private boolean timeout = false;
    private Long maxDelay = 3000L; 
    /**
     * 
     */
    private int winRate = 2000;
    
//    private 
    
    /**  Le nombre de noeuds d�velopp� par l'algorithme
     * (int�ressant pour se faire une id�e du nombre de noeuds d�velopp�s) */
    private int nbnoeuds;

    /** Le nombre de feuilles �valu�es par l'algorithme
     */
    private int nbfeuilles;
    
	public AlphaBeta(Heuristique h, String joueurMax, String joueurMin) {
		this(h,joueurMax,joueurMin,PROFMAXDEFAUT);
	}

	public AlphaBeta(Heuristique h, String joueurMax, String joueurMin, int profMaxi) {
		this.h = h;
        this.joueurMin = joueurMin;
        this.joueurMax = joueurMax;
        profMax = profMaxi;
		System.out.println("Initialisation d'un AlphaBeta de profondeur " + profMax);
	}
	
	
	// -------------------------------------------
	  // Méthodes de l'interface AlgoJeu
	  // -------------------------------------------
	   public String meilleurCoup(PlateauCam p) {
		   timeout = false;
		   startingTime = System.currentTimeMillis();
	        /* A vous de compléter le corps de ce fichier */
		   heurMax = Integer.MIN_VALUE;
		   
		   ArrayList<String> coups = p.coupsPossibles(joueurMax);
		   
		   System.out.print("-- " + coups.size() + " Coups: \n");
		   int alpha = -winRate;
		   int beta = winRate;
		   
		   Node genesis = new Node(p, Integer.MIN_VALUE, "", joueurMax);
		   ArrayList<Node> sortedChilds = genesis.sortedChilds(h, 0);
		   ABEntry entry;
		   
//		   System.out.println(p.toString());
		   int prof = 0;
		   while(!timeout) {
			   System.out.println("-- Profondeur " + prof + ", meilleur coup [" +bestMove+ "] : " + alpha);
//			   System.out.println(best.toString());
			   for (Node child : sortedChilds) {
				   entry = new ABEntry(child, prof);
				   Integer heur = heurMap.get(entry);
				   if (heur == null) {
					   alpha = Math.max(alpha, alphaBeta(child, prof, alpha, beta, 0, joueurMin));
					   heurMap.put(entry, alpha);
				   } else {
					   System.out.println(child.getInitialMove() + " trouvé avec " + heur);
					   alpha = heur;
				   }
				   
				   if (alpha > heurMax) {
		    			heurMax = alpha;
		    			bestMove = child.getInitialMove();
		    		}
				   timeout = (System.currentTimeMillis() - startingTime >= maxDelay);
			   }
			   ++prof;
		   }
		   
		   System.out.println();
		   System.out.println("-- " + nbnoeuds + " noeuds, " + coupe + " coupes.");
		   System.out.println("-- Gains max " + heurMax);
		   coupe = 0;
		   return bestMove;
	    }

	  // -------------------------------------------
	  // Méthodes publiques
	  // -------------------------------------------
	    public String toString() {
	        return "AlphaBeta(ProfMax="+profMax+")";
	    }

	  // -------------------------------------------
	  // Méthodes internes
	  // -------------------------------------------

	    //A vous de jouer pour implanter AlphaBeta

	    private int alphaBeta(Node n, int prof, int alpha, int beta, int nbCoups, String player) {
	    	if (n.getPlateau().win(player)) {
	    		best = n.getPlateau();
	    		return (player.equals(joueurMax)) ? winRate : -winRate;
	    	} else if (prof <= 0) {
	    		++nbfeuilles;
	    		return n.getScore();
	    	}
	    	
	    	if (player.equals(joueurMax)) {
	    		for (final Node child : n.sortedChilds(h, nbCoups+1)) {
	    			if (timeout)
	    				return alpha;
	    			ABEntry entry = new ABEntry(child, prof-1);
					Integer heur = heurMap.get(entry);
	    			if (heur == null) {
	    				alpha = Math.max(alpha, alphaBeta(child, prof-1, alpha, beta, nbCoups+1, joueurMin));
						   heurMap.put(entry, alpha);
					   } else {
						   System.out.println(child.getInitialMove() + " trouvé avec " + heur);
						   alpha = heur;
					   }
	    			++nbnoeuds;
	    			if (beta <= alpha) {
	    				coupe++;
	    				break;
	    			}
	    			timeout = (System.currentTimeMillis() - startingTime >= maxDelay);
	    		}
				return alpha;
	    	} else {
	    		for (final Node child : n.reverseSortedChilds(h, nbCoups+1)) {
	    			if (timeout)
	    				return beta;
	    			ABEntry entry = new ABEntry(child, prof-1);
					Integer heur = heurMap.get(entry);
	    			if (heur == null) {
	    				beta = Math.min(beta, alphaBeta(child, prof-1, alpha, beta, nbCoups+1, joueurMax));
						   heurMap.put(entry, alpha);
					   } else {
						   System.out.println(child.getInitialMove() + " trouvé avec " + heur);
						   beta = heur;
					   }
	    			++nbnoeuds;
	    			if (beta <= alpha) {
	    				coupe++;
	    				break;
	    			}
	    			timeout = (System.currentTimeMillis() - startingTime >= maxDelay);
	    		}
				return beta;
	    	}
	    }
		
		public void setProfMax(int profMax) {
			this.profMax = profMax;
		}
}
