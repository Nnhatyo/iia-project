package cam;

import java.util.ArrayList;
import java.util.Collections;

import Game.PlateauCam;

public class Node implements Comparable<Node> {
	private PlateauCam plateau;
	private Integer score;
	private String initialMove;
	private String player;
	
	public Node(PlateauCam p, Integer s, String i, String pl) {
		plateau = p;
		score = s;
		initialMove = i;
		player = pl;
	}
	
	@Override
	public int compareTo(Node o) {
		 if(score > o.getScore())
			 return -1;
		 if (score < o.getScore())
			 return 1;
		 return 0;
	}
	
	@Override
	public int hashCode() {
		return (plateau.getPlateau().hashCode() + score + player.hashCode());
	}
	
	public boolean equals(Object other) {
		Node e = (Node) other;
		
		boolean eq = e.plateau.getPlateau().equals(plateau.getPlateau());
		eq = eq && (score.equals(e.getScore()));
		eq = eq && (initialMove.equals(e.getInitialMove()));
		eq = eq && (player.equals(e.getPlayer()));
		
		return eq;
	}
	
	public Integer getScore() {
		return score;
	}
	
	public PlateauCam getPlateau() {
		return plateau;
	}
	
	public String getInitialMove() {
		return initialMove;
	}
	
	public String getPlayer() {
		return player;
	}
	
	public ArrayList<Node> reverseSortedChilds(Heuristique h, int nbCoups) {
		ArrayList<Node> childs = new ArrayList<>();
		String player2 = (player.equals("blanc")) ? "noir" : "blanc";
		
		for (final String coup : plateau.coupsPossibles(player)) {
			PlateauCam p2 = plateau.copy().playRet(coup, player);
			h.updateStrategy(p2, player2);
			childs.add(new Node(p2, h.eval(p2, player, nbCoups), coup, player2));
		}
		
		Collections.sort(childs);
		Collections.reverse(childs);
		
		return childs;
	}
	
	public ArrayList<Node> sortedChilds(Heuristique h, int nbCoups) {
		ArrayList<Node> childs = new ArrayList<>();
		String player2 = (player.equals("blanc")) ? "noir" : "blanc";
		
		for (final String coup : plateau.coupsPossibles(player)) {
			PlateauCam p2 = plateau.copy().playRet(coup, player);
			h.updateStrategy(p2, player2);
			childs.add(new Node(p2, h.eval(p2, player, nbCoups), coup, player2));
		}
		
		Collections.sort(childs);
		
		return childs;
	}
}
