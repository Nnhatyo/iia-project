package cam;

import java.util.ArrayList;
import java.util.Random;

import Game.PlateauCam;

public class ChuckMaurice implements IJoueur {
	static final int BLANC = -1;
    static final int NOIR = 1;
    static final int VIDE = 0;
    static AlphaBeta algo;
    
	int intPlayer;
	String player;
	String coup;
	ArrayList<String> coups = new ArrayList<>();
	PlateauCam plateau = new PlateauCam();
	String name;
	
	public ChuckMaurice() {
		String[] names = {"Chuck", "Panthéon", "Tchugo", "Yamamoto", "Tweedz", "Roger", "Bernard", "Fromageance", "MasterCam"};
		Random rd = new Random(this.hashCode());
		name = names[rd.nextInt(names.length-1)];
	}
	
	@Override
	public void initJoueur(int mycolour) {
		// TODO Auto-generated method stub
		intPlayer = mycolour;
		switch (intPlayer) {
		case BLANC:
			player = "blanc";
			algo = new AlphaBeta(new Strat2(), "blanc", "noir");
			plateau.countPieces();
			break;
		case NOIR:
			player = "noir";
			algo = new AlphaBeta(new Strat2(), "noir", "blanc");
			break;
		default:
			player = "undefined";
			break;
		}
	}

	@Override
	public int getNumJoueur() {
		// TODO Auto-generated method stub
		return intPlayer;
	}

	@Override
	public String choixMouvement() {
		// TODO Auto-generated method stub
//		Random rd = new Random();
		
		int profMax = 10 - plateau.nbWhite - plateau.nbBlack;
		profMax = Math.min(7, Math.max(profMax, 4));
//		System.out.println("nouvelle profondeur: " + profMax);
		algo.setProfMax(profMax);
		System.out.println("Profondeur " + profMax);
		System.out.println("\t" + plateau.nbWhite + " blancs");
//		System.out.println("\t" + plateau.nbWhiteE + " East");
//		System.out.println("\t" + plateau.nbWhiteW + " West");
//		System.out.println("\t" + plateau.nbWhiteC + " Center");
		System.out.println("\t" + plateau.nbBlack + " noirs");
//		System.out.println("\t" + plateau.nbBlackE + " East");
//		System.out.println("\t" + plateau.nbBlackW + " West");
//		System.out.println("\t" + plateau.nbBlackC + " Center");
		coups = plateau.coupsPossibles(player);
		System.out.println("---------------------------------------");
		System.out.println("je suis " + player.toUpperCase());
		System.out.println(plateau.toString());
		coup = algo.meilleurCoup(plateau);
//		coup = coups.get(rd.nextInt(coups.size()));
		
		plateau.play(coup, player);
		System.out.println("j'ai joué " + coup);
		System.out.println(plateau.toString());
		System.out.println("---------------------------------------");
		return coup;
	}

	@Override
	public void declareLeVainqueur(int colour) {
		// TODO Auto-generated method stub
		switch (colour) {
		case BLANC:
			System.out.println("Blanc a gagné !");
			break;
		case NOIR:
			System.out.println("Noir a gagné !");
			break;
		default:
			System.out.println("Personne n'a gagné.");
			break;
		}

	}

	@Override
	public void mouvementEnnemi(String coup) {
		coups.clear();
		// TODO Auto-generated method stub
		switch (-intPlayer) {
		case BLANC:
			plateau.play(coup, "blanc");
			plateau.countPieces();
			break;
		case NOIR:
			plateau.play(coup, "noir");
			plateau.countPieces();
			break;
		default:
			break;
		}
		System.out.println(coup);
		System.out.println(plateau.toString());
	}

	@Override
	public String binoName() {
		// TODO Auto-generated method stub
		return name;
	}
}
