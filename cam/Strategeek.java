package cam;

import java.util.Random;

import Game.PlateauCam;

public class Strategeek implements Heuristique {

	public Strategeek() {
	}
	
	/////////////////////////////////////////////////
	// ---------- Strategy ----------
	private static int castleRush = 5;
	private static int castleDefense = 1;
	private static int defense = 3;
	private static int offense = 2;
	private static int side = 0;
	private static int center = 0;

	

	public int eval(PlateauCam p, byte player, int nbCoups) {
//		System.out.println("On evalue:");
//		System.out.println(p.toString());

		int heuristique = 0;
		
		if ( player == PlateauCam.M_BLANC ) {
			// Heuristique for white player
			{
//				if (PlateauCam.mask(p.getCase(12, 3), PlateauCam.M_CHATEAU) == 0) {
//					heuristique += 2000;
//				}
				heuristique += defense;// * p.nbWhite;
				heuristique += -offense;// * p.nbBlack;
//				heuristique += side * (p.nbBlackE + p.nbBlackW);
//				heuristique += center * p.nbWhiteC;
				heuristique += castleDefense * p.distanceBlack;
				heuristique += -castleRush * p.distanceWhite;
			}
//			{
//				heuristique += p.nbWhite * defense;
//				heuristique -= p.nbBlack * offense;
//				heuristique += (p.nbWhiteC - p.nbBlackC);
//				heuristique -= ((p.nbWhiteE + p.nbWhiteW) -  (p.nbBlackE + p.nbBlackW));
//				heuristique += p.distanceBlack * castleDefense;
//				heuristique -= p.distanceWhite * castleRush;
//			}
		} else {
			// Heuristique for black player
			{
//				if (PlateauCam.mask(p.getCase(0, 3), PlateauCam.M_CHATEAU) == 0) {
//					heuristique += 2000;
//				}
				heuristique += defense;// * p.nbBlack;
				heuristique += -offense;// * p.nbWhite;
//				heuristique += side * (p.nbWhiteE + p.nbWhiteW);
//				heuristique += center * p.nbBlackC;
				heuristique += castleDefense * p.distanceWhite;
				heuristique += -castleRush * p.distanceBlack;
			}
//			{
//				heuristique += p.nbBlack * defense; // defense
//				heuristique -= p.nbWhite * offense; // offense
//				heuristique += (p.nbBlackC - p.nbWhiteC); // center
//				heuristique -= ((p.nbBlackE + p.nbBlackW) - (p.nbWhiteE + p.nbWhiteW)); // side
//				heuristique += p.distanceWhite * castleDefense; // castle defense
//				heuristique -= p.distanceBlack * castleRush; // castle rush
//			}
		}
		heuristique -= nbCoups;
//		System.out.println("heuristique: " + heuristique);
		return heuristique;
	}

	public int eval(PlateauCam p, IJoueur player, int nbCoups) {
		if (player.getNumJoueur() == IJoueur.BLANC) {
			return eval(p, PlateauCam.M_BLANC, nbCoups);
		} else {
			return eval(p, (byte) ~PlateauCam.M_BLANC, nbCoups);
		}
	}
	
	public int eval(PlateauCam p, String player, int nbCoups) {
		if (player.equals("blanc")) {
			return eval(p, PlateauCam.M_BLANC, nbCoups);
		} else {
			return eval(p, (byte) ~PlateauCam.M_BLANC, nbCoups);
		}
	}

	@Override
	public void updateStrategy(PlateauCam p, String player) {
		p.countPieces();
		Random rd = new Random();
//		castleRush = 5;
//		castleDefense = 1;
//		defense = 2;
//		offense = 3;
//		side = 2;
//		center = 1;
		
		switch (player) {
		case "blanc":
//			{
				castleRush = (p.distanceBlack != 0) ? p.distanceWhite / p.distanceBlack : 0;
//				castleRush *= (rd.nextInt(5) + 1);
				castleDefense = p.nbBlackS * p.distanceBlack;
				defense = (p.nbBlack != 0) ? (p.nbWhite / p.nbBlack) * (rd.nextInt(2) + 1) : (rd.nextInt(2) + 1);
				offense = (p.nbWhite != 0) ? p.nbBlack / p.nbWhite : 0;
				side = ((p.nbBlackE + p.nbBlackW) != 0) ? p.nbBlackC / (p.nbBlackE + p.nbBlackW) : 0;
				center = (p.nbBlackC != 0) ? (p.nbBlackE + p.nbBlackW) / p.nbBlackC : 0;
//			}
			break;
		case "noir":
//			{
				castleRush = (p.distanceWhite != 0) ? p.distanceBlack / p.distanceWhite : 0;
//				castleRush *= (rd.nextInt(5) + 1);
				castleDefense = p.nbWhiteN * p.distanceWhite;
				defense = (p.nbWhite != 0) ? (p.nbBlack / p.nbWhite) * (rd.nextInt(2) + 1) : (rd.nextInt(2) + 1);
				offense = (p.nbBlack != 0) ? p.nbWhite / p.nbBlack : 0;
				side = ((p.nbWhiteE + p.nbWhiteW) != 0) ? p.nbWhiteC / (p.nbWhiteE + p.nbWhiteW) : 0;
				center = (p.nbWhiteC != 0) ? (p.nbWhiteE + p.nbWhiteW) / p.nbWhiteC : 0;
//			}
			break;
		default:
			break;
		}
	}
	
	
}