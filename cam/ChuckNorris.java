package cam;

import java.util.Random;

import GUI.GameFrame;
import Game.PlateauCam;

public class ChuckNorris implements IJoueur {

	static final int BLANC = -1;
    static final int NOIR = 1;
    static final int VIDE = 0;
    static AlphaBeta algo;
    
	int intPlayer;
	String player;
	String coup;
	PlateauCam plateau = new PlateauCam();
	String name;
	GameFrame camFrame;
	
	public ChuckNorris() {
		String[] names = {"Panthéon", "Tweedz", "Roger", "Bernard", "La Fromageance", "MasterCam", "Cam Ehameha",
		"Le Robinet"};
		Random rd = new Random(this.hashCode());
		rd.nextInt();
		name = names[rd.nextInt(names.length-1)];
		camFrame = new GameFrame(plateau);
	}
	
	@Override
	public void initJoueur(int mycolour) {
		intPlayer = mycolour;
		switch (intPlayer) {
		case BLANC:
			player = "blanc";
			algo = new AlphaBeta(new Strategeek(), "blanc", "noir");
			plateau.countPieces();
			break;
		case NOIR:
			player = "noir";
			algo = new AlphaBeta(new Strategeek(), "noir", "blanc");
			break;
		default:
			player = "undefined";
			break;
		}
	}

	@Override
	public int getNumJoueur() {
		return intPlayer;
	}

	@Override
	public String choixMouvement() {		
		int profMax = 15 - plateau.nbWhite - plateau.nbBlack;
		profMax = Math.min(12, Math.max(profMax, 6));
		algo.setProfMax(profMax);
		System.out.println("---------------------------------------");
		System.out.println("-- Nouvelle profondeur " + profMax);
		System.out.println("-- Je suis " + player.toUpperCase());
//		System.out.println(plateau.toString());
		coup = algo.meilleurCoup(plateau);
		
		plateau.play(coup, player);
		System.out.println("-- J'ai joué [" + coup + "]");
		System.out.println(plateau.toString());
		camFrame.update();
		return coup;
	}

	@Override
	public void declareLeVainqueur(int colour) {
		switch (colour) {
		case BLANC:
			System.out.println("Blanc a gagné !");
			break;
		case NOIR:
			System.out.println("Noir a gagné !");
			break;
		default:
			System.out.println("Personne n'a gagné.");
			break;
		}
		camFrame.dispose();
	}

	@Override
	public void mouvementEnnemi(String coup) {
		switch (intPlayer) {
		case NOIR:
			plateau.play(coup, "blanc");
			plateau.countPieces();
			break;
		case BLANC:
			plateau.play(coup, "noir");
			plateau.countPieces();
			break;
		default:
			break;
		}
		System.out.println(coup);
		System.out.println(plateau.toString());
		System.out.println("---------------------------------------");
		camFrame.update();
	}

	@Override
	public String binoName() {
		return name;
	}
}