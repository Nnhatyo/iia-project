package cam;

import Game.PlateauCam;

public interface Heuristique {
	public int eval(PlateauCam p, IJoueur player, int nbCoups);

	public int eval(PlateauCam p, String joueurMin, int nbCoups);

	public void updateStrategy(PlateauCam p, String joueurMax);
}
