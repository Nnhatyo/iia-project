package cam;

import java.util.ArrayList;
import java.util.Random;

import Game.PlateauCam;

public class JoueurAleatoire implements IJoueur {

	static final int BLANC = -1;
    static final int NOIR = 1;
    static final int VIDE = 0;
    
	int intPlayer;
	String player;
	String coup;
	ArrayList<String> coups = new ArrayList<>();
	PlateauCam plateau = new PlateauCam();
	
	@Override
	public void initJoueur(int mycolour) {
		// TODO Auto-generated method stub
		intPlayer = mycolour;
		switch (intPlayer) {
		case BLANC:
			player = "blanc";
			break;
		case NOIR:
			player = "noir";
			break;
		default:
			player = "undefined";
			break;
		}
	}

	@Override
	public int getNumJoueur() {
		// TODO Auto-generated method stub
		return intPlayer;
	}

	@Override
	public String choixMouvement() {
		// TODO Auto-generated method stub
		coups = plateau.coupsPossibles(player);
		Random rd = new Random();
		coup = coups.get(rd.nextInt(coups.size()));
		plateau.play(coup, player);
		return coup;
	}

	@Override
	public void declareLeVainqueur(int colour) {
		// TODO Auto-generated method stub
		switch (colour) {
		case BLANC:
			System.out.println("Blanc a gagné !");
			break;
		case NOIR:
			System.out.println("Noir a gagné !");
			break;
		default:
			System.out.println("Personne n'a gagné.");
			break;
		}

	}

	@Override
	public void mouvementEnnemi(String coup) {
		coups.clear();
		// TODO Auto-generated method stub
		switch (-intPlayer) {
		case BLANC:
			plateau.play(coup, "blanc");
			break;
		case NOIR:
			plateau.play(coup, "noir");
			break;
		default:
			break;
		}
	}

	@Override
	public String binoName() {
		// TODO Auto-generated method stub
		return "Toto";
	}

}
